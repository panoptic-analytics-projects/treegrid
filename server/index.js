const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const  fs  = require("fs");
const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer);
const Data=require("./data.json")


io.on("connection", (socket) => {
    socket.on("syncData",(args) => {
       fs.writeFileSync("./data.json",JSON.stringify(args))
 })
 // Send the entire list
        socket.emit('list', {
        type: 'SET',
        data: Data
    });

   // Add the item and send it to everyone
  socket.on('list:add', item => {
    fs.writeFileSync("./data.json",JSON.stringify(item))
    io.sockets.emit('list', {
      type: 'ADD',
      data: item
    });
  });

  // Remove the item and send the id to everyone
  socket.on('list:remove', id => {
    list = Data.filter(item => item.EmployeeID !== id);
    fs.writeFileSync(list) 
    io.sockets.emit('list', {
      type: 'REMOVE',
      ids : id
    });
  });

  socket.on("Updat:list",id =>{
      const updatedvalue=Data.find(current => current.EmployeeID  === id)
       fs.writeFileSync(updatedvalue)
      io.sockets.emit('list',  {
        type: 'UPDATE',
        ids : id,
        data: updatedvalue
      });
  });
  

});

httpServer.listen(3000);

module.exports = app;





// app.get("/",(req,res)=>{
//     fs.readFile("./data.json","utf8",(err,data)=>{
//         res.send(JSON.parse(data));
//     })
// })

app.get('/filter', (req, res, next) => {
    const filters = req.query;
    const filteredUsers = data.filter(user => {
      let isValid = true;
      for (key in filters) {
        isValid = isValid && user[key] == filters[key];
      }
      return isValid;
    });
    res.send(filteredUsers);
  });

app.listen(2000,()=>{
    console.log("app listening at http://localhost:${port}")
});


// httpServer.listen(3000);